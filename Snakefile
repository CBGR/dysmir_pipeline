################################################################
## Import functions 
import os
import itertools
from snakemake.utils import R

#configfile: "config.yaml"
#configfile: "/storage/scratch/TCGA/StJude/PCGP/dysmir_input/Separated_samples/BLLETV6RUNX1/config.yaml"
#configfile: "/storage/scratch/TCGA/StJude/PCGP/dysmir_input/Separated_samples/BLLRGA/config.yaml"
configfile: "/storage/scratch/TCGA/StJude/PCGP/dysmir_input/Separated_samples/HGGNOS/config.yaml"


################################################################
## Output files + directories
################################################################
################################################################

MAIN_DIR_ABS = os.getcwd()
RESULTS_DIR = os.path.join(config["title"], "results")
DATA_DIR = os.path.join(config["title"], "data")


################################################################
## ENSEMBL protein coding genes table
ENSEMBL_TAB = os.path.join(DATA_DIR, "PCGs", "ENSEMBL_protein_coding_genes.tab")
ENSEMBL_RDATA = os.path.join(DATA_DIR, "PCGs", "ENSEMBL_protein_coding_genes.Rdata")


################################################################
## TFBS associated to CREs and proximal TSSs
#ASSOCIATED_TFBSs = expand(os.path.join(RESULTS_DIR, "Associated_TFBSs", "TFBS_associated_with_{cre}_target_genes_PCG_miRNA_{hg}.bed"), cre = config["CRE_database"], hg = config["human_genome_version"])
ASSOCIATED_TFBSs = expand(os.path.join(RESULTS_DIR, "Associated_TFBSs", "TFBS_associated_with_{cre}_target_genes_PCG_miRNA_{hg}.RData"), cre = config["CRE_database"], hg = config["human_genome_version"])


################################################################
## Mutated TFBSs
MAPPED_MUTATIONS_IN_PCG_TFBS = os.path.join(RESULTS_DIR, "Mutated_TFBSs", "Mutated_TFBSs_associated_to_PCGs.bed")
MAPPED_MUTATIONS_IN_PCG_TFBS_FILTERED = os.path.join(RESULTS_DIR, "Mutated_TFBSs", "Mutated_TFBSs_associated_to_PCGs_filtered.bed")
MAPPED_MUTATIONS_IN_miRNA_TFBS = os.path.join(RESULTS_DIR, "Mutated_TFBSs", "Mutated_TFBSs_associated_to_miRNAs.bed")


################################################################
## xseq results - miRNA
XSEQ_MIRNA_TAB = expand(os.path.join(RESULTS_DIR, "xseq", "miRNA", "trans", "RData", "xseq_trans_miRNA_TFBS_{cohort}.Rdata"), cohort = config['cohort_name'])
XSEQ_MIRNA_POSTPROB_TAB = expand(os.path.join(RESULTS_DIR, "xseq", "miRNA", "trans", "RData", "xseq_trans_miRNA_TFBS_post_prob_{cohort}.Rdata"), cohort = config['cohort_name'])
XSEQ_MIRNA_NET = expand(os.path.join(RESULTS_DIR, "xseq", "miRNA", "trans", "RData", "{cohort}_miRNA_network_filtered.Rdata"), cohort = config['cohort_name'])


################################################################
## xseq results - PCG
XSEQ_PCG_TAB = expand(os.path.join(RESULTS_DIR, "xseq", "PCG", "trans", "RData", "xseq_trans_PCG_TFBS_{cohort}.Rdata"), cohort = config['cohort_name'])
XSEQ_PCG_POSTPROB_TAB = expand(os.path.join(RESULTS_DIR, "xseq", "PCG", "trans", "RData", "xseq_trans_PCG_TFBS_post_prob_{cohort}.Rdata"), cohort = config['cohort_name'])
XSEQ_PCG_NET = expand(os.path.join(RESULTS_DIR, "xseq", "PCG", "trans", "RData", "{cohort}_PCG_network_filtered.Rdata"), cohort = config['cohort_name'])


################################################################
## Selected driver genes: PCGs + miRNAs
DRIVERS_PCG = expand(os.path.join(RESULTS_DIR, "selected_genes_and_plots", "PCG", "selected_samples", "Filtered_xseq_post_prob_{cohort}_PCGs.tab"), cohort = config['cohort_name'])
DRIVERS_MIRNA = expand(os.path.join(RESULTS_DIR, "selected_genes_and_plots", "miRNA", "selected_samples", "Filtered_xseq_post_prob_{cohort}_miRNAs.tab"), cohort = config['cohort_name'])


################################################################
## Tables with GO/GSEA Enriched terms
GENE_TYPES = ""
if(config['RNAseq_tab'] and config['miRNAseq_tab']):
    GENE_TYPES = 'PCG miRNA'.split()

    
if(config['RNAseq_tab'] and not config['miRNAseq_tab']):
    GENE_TYPES = 'PCG'.split()
    
ENRICHR_TABLE = expand(os.path.join(RESULTS_DIR, "functional_enrichment_analysis", "{genes}", "tables", "Enriched_terms_{genes}_{cohort}.tab"), cohort = config['cohort_name'], genes = GENE_TYPES)


################################################################
## Interactive reports
INTERACTIVE_REPORT = expand(os.path.join(RESULTS_DIR, "Report", "{genes}", "dysmiR_analysis_report_{cohort}_{genes}.html"), cohort = config['cohort_name'], genes = GENE_TYPES)



################################################################
################################################################
## Rules
if(config['RNAseq_tab'] and config['miRNAseq_tab']):
    rule all: 
        input:
            #ENSEMBL_TAB, \     
            #ENSEMBL_RDATA, \
	    #ASSOCIATED_TFBSs, \
	    #MAPPED_MUTATIONS_IN_PCG_TFBS, \
	    #MAPPED_MUTATIONS_IN_miRNA_TFBS, \
            #MAPPED_MUTATIONS_IN_PCG_TFBS_FILTERED, \
            #XSEQ_MIRNA_TAB, \
            #XSEQ_MIRNA_POSTPROB_TAB, \
	    #XSEQ_MIRNA_NET, \
	    #XSEQ_PCG_TAB, \
	    #XSEQ_PCG_POSTPROB_TAB, \
	    #XSEQ_PCG_NET, \
            #DRIVERS_PCG, \
            #DRIVERS_MIRNA, \
            #ENRICHR_TABLE, \
            INTERACTIVE_REPORT, \


if(config['RNAseq_tab'] and not config['miRNAseq_tab']):
    rule all: 
        input:
            #ENSEMBL_TAB, \     
            #ENSEMBL_RDATA, \
	    #ASSOCIATED_TFBSs, \
	    #MAPPED_MUTATIONS_IN_PCG_TFBS, \
	    #MAPPED_MUTATIONS_IN_PCG_TFBS_FILTERED, \
            #XSEQ_PCG_TAB, \
	    #XSEQ_PCG_POSTPROB_TAB, \
	    #XSEQ_PCG_NET, \
            #DRIVERS_PCG, \
            #ENRICHR_TABLE, \
            INTERACTIVE_REPORT, \


################################################################
## Protein Coding Gene list from ENSEMBL
################################################################
rule Generate_ENSEMBL_list_of_protein_coding_genes:
    """ 
    Generate a table and a Rdata object with the list of protein coding genes taken from ENSEMBL.
    """
    input:
        config["RNAseq_tab"]
    output:
        ENSEMBL_TAB, \
        ENSEMBL_RDATA
    message:
        "; Exporting ENSEMBL table with protein coding genes"
    params:
        results_dir = os.path.join(DATA_DIR, "PCGs"), \
        R_scripts = config["R_scripts"]
    shell:
        """
        Rscript {params.R_scripts}/List_of_ENSEMBL_protein_coding_genes.R \
         -g GRCh37 \
         -o {params.results_dir}
        """


################################################################
## Associate TFBSs with target genes
################################################################
rule Associate_regions_with_target_genes:
     """ 
      Create a BED file with the Regions-TSS associations.
     """
     input:
         "data/TFBS/TFBSs_ChIP_eat_enrichment_zone_hg38.RData"
     output:
         ASSOCIATED_TFBSs
     message:
        "; Associating TFBSs with target genes "
     params:
        results_dir = os.path.join(RESULTS_DIR, "Associated_TFBSs"), \
        R_scripts = config["R_scripts"], \
	genome = config["human_genome_version"], \
	cre = config["CRE_database"], \
	feature_name = "TFBS", \
	data_dir = config["data_dir"]
     shell:
         """
         Rscript {params.R_scripts}/Associate_genomic_coordinates_with_target_genes.R \
          -c {params.cre} \
          -d {params.data_dir} \
	  -n {params.feature_name} \
	  -g {params.genome} \
	  -o {params.results_dir} \
	  -f {input}
         """


################################################################
## Map mutations within TFBSs
################################################################
rule Map_mutations_within_TFBSs:
     """ 
      Map the mutations within a set of associated TFBSs
     """
     input:
         Mutations = config['WGS_tab'], \
         TFBSs = ASSOCIATED_TFBSs
     output:
         MAPPED_MUTATIONS_IN_PCG_TFBS, \
         MAPPED_MUTATIONS_IN_miRNA_TFBS
     message:
         "; Mapping mutations in associated TFBSs"
     params:
         results_dir = os.path.join(RESULTS_DIR, "Mutated_TFBSs"), \
         R_scripts = config["R_scripts"], \
         cohort_name = config["cohort_name"]
     shell:
         """
         Rscript {params.R_scripts}/Map_mutations_in_TFBSs.R \
          -c {params.cohort_name} \
	  -m {input.Mutations} \
	  -t {input.TFBSs} \
	  -o {params.results_dir}
         """


################################################################
## Filter mutated TFBSs associated to PCGs
################################################################
rule Filter_TFBS_associated_to_PCGs:
     """ 
      For each sample, keep the TFBSs whose target genes are expressed above or below its cohort mean expression.
     """
     input:
         Mutations = MAPPED_MUTATIONS_IN_PCG_TFBS, \
         Expression = config['RNAseq_tab']
     output:
         MAPPED_MUTATIONS_IN_PCG_TFBS_FILTERED
     message:
         "; Filtering TFBSs mutations"
     params:
         results_dir = os.path.join(RESULTS_DIR, "Mutated_TFBSs"), \
         R_scripts   = config["R_scripts"], \
         cohort_name = config["cohort_name"], \
         sd          = config['sd_expr'], \
	 filter_flag = config['filter_TFBS']
     shell:
         """
         Rscript {params.R_scripts}/Filter_TFBS_mutations_with_gene_expression.R \
	  -m {input.Mutations} \
	  -e {input.Expression} \
	  -o {params.results_dir} \
	  -f {params.filter_flag} \
	  -s {params.sd}
         """


################################
## Rund xseq trans for miRNAs ##
################################
rule xseq_trans_miRNA:
    """ 
    Run xseq (trans analysis) for the miRNA genes
    """
    input:
        mutation_file = MAPPED_MUTATIONS_IN_miRNA_TFBS, \
        mirna_network = config['miRNA_network'], \
        expr_pcg_tab_file = config['RNAseq_tab'], \
        expr_mirna_tab_file = config['miRNAseq_tab'], \
        mir_premir_tab_file = config['miRNA_premiRNA_dictionary'], \
        cna_pcg_tab = config['CNA_pcg_tab'], \
        cna_pcg_log_tab = config['CNA_pcg_log_tab']
    output:
        xseq_table = os.path.join(RESULTS_DIR, "xseq", "miRNA", "trans", "RData", "xseq_trans_miRNA_TFBS_{cohort}.Rdata"), \
        xseq_postprob_table = os.path.join(RESULTS_DIR, "xseq", "miRNA", "trans", "RData", "xseq_trans_miRNA_TFBS_post_prob_{cohort}.Rdata"), \
	net = os.path.join(RESULTS_DIR, "xseq", "miRNA", "trans", "RData", "{cohort}_miRNA_network_filtered.Rdata")
    message:
        "; Running xseq (trans) miRNA genes"
    params:
        results_dir = os.path.join(RESULTS_DIR, "xseq", "miRNA", "trans"), \
        R_scripts = config["R_scripts"], \
        cohort_name = config["cohort_name"], \
        max_conn = config["max_conn_mirna"], \
        min_conn = config["min_conn"], \
        conn_weight = config["connection_weight"], \
        DEG = config["DEG_pvalue"], \
        exp_weight = config["expression_weight"]
    shell:
        """
        Rscript {params.R_scripts}/xseq_trans_miRNA.R  \
        -n {input.mirna_network} \
        -m {input.mutation_file} \
        -r {input.expr_pcg_tab_file} \
        -s {input.expr_mirna_tab_file} \
        -p {input.mir_premir_tab_file} \
	-o {params.results_dir} \
        -a {input.cna_pcg_tab} \
        -l {input.cna_pcg_log_tab} \
        -c {params.cohort_name} \
        -x {params.max_conn} \
        -i {params.min_conn} \
        -t {params.conn_weight} \
        -w {params.exp_weight} \
        -d {params.DEG} \
        -z {params.R_scripts}
        """


###############################################################
## Rund xseq trans for PCGs                                  ##
## The input changes depending if the CNA files are provided ##
###############################################################
if (not config['CNA_pcg_log_tab'] or not config['CNA_pcg_log_tab']):
    
    rule xseq_trans_PCG:
        """ 
        Run xseq (trans analysis) for the PCGs
        """
        input:
            mutation_file = MAPPED_MUTATIONS_IN_PCG_TFBS_FILTERED, \
            pcg_network = config['PCG_network'], \
            expr_pcg_tab_file = config['RNAseq_tab']
        output:
            xseq_table = os.path.join(RESULTS_DIR, "xseq", "PCG", "trans", "RData", "xseq_trans_PCG_TFBS_{cohort}.Rdata"), \
            xseq_postprob_table = os.path.join(RESULTS_DIR, "xseq", "PCG", "trans", "RData", "xseq_trans_PCG_TFBS_post_prob_{cohort}.Rdata"), \
            net = os.path.join(RESULTS_DIR, "xseq", "PCG", "trans", "RData", "{cohort}_PCG_network_filtered.Rdata")
        message:
            "; Running xseq (trans) PCGs"
        params:
            results_dir   = os.path.join(RESULTS_DIR, "xseq", "PCG", "trans"), \
            R_scripts     = config["R_scripts"], \
            cohort_name   = config["cohort_name"], \
            max_conn      = config["max_conn_pcg"], \
            min_conn      = config["min_conn"], \
            conn_weight   = config["connection_weight"], \
            DEG           = config["DEG_pvalue"], \
	    impute        = config["impute_flag"], \
            exp_weight    = config["expression_weight"]
        shell:
            """
            Rscript {params.R_scripts}/xseq_trans_PCG.R  \
            -n {input.pcg_network} \
            -m {input.mutation_file} \
            -r {input.expr_pcg_tab_file} \
	    -o {params.results_dir} \
            -c {params.cohort_name} \
            -x {params.max_conn} \
            -i {params.min_conn} \
            -t {params.conn_weight} \
            -w {params.exp_weight} \
	    -j {params.impute} \
            -d {params.DEG} \
            -z {params.R_scripts}
            """

elif (config['CNA_pcg_log_tab'] and config['CNA_pcg_log_tab']):

    rule xseq_trans_PCG:
        """ 
        Run xseq (trans analysis) for the PCGs
        """
        input:
            mutation_file = MAPPED_MUTATIONS_IN_PCG_TFBS_FILTERED, \
            pcg_network = config['PCG_network'], \
            expr_pcg_tab_file = config['RNAseq_tab'], \
            cna_pcg_tab = config['CNA_pcg_tab'], \
            cna_pcg_log_tab = config['CNA_pcg_log_tab']
        output:
            xseq_table = os.path.join(RESULTS_DIR, "xseq", "PCG", "trans", "RData", "xseq_trans_PCG_TFBS_{cohort}.Rdata"), \
            xseq_postprob_table = os.path.join(RESULTS_DIR, "xseq", "PCG", "trans", "RData", "xseq_trans_PCG_TFBS_post_prob_{cohort}.Rdata"), \
            net = os.path.join(RESULTS_DIR, "xseq", "PCG", "trans", "RData", "{cohort}_PCG_network_filtered.Rdata")
        message:
            "; Running xseq (trans) PCGs"
        params:
            results_dir = os.path.join(RESULTS_DIR, "xseq", "PCG", "trans"), \
            R_scripts = config["R_scripts"], \
            cohort_name = config["cohort_name"], \
            max_conn = config["max_conn_pcg"], \
            min_conn = config["min_conn"], \
            conn_weight = config["connection_weight"], \
            DEG = config["DEG_pvalue"], \
            exp_weight = config["expression_weight"]
        shell:
            """
            Rscript {params.R_scripts}/xseq_trans_PCG.R  \
            -n {input.pcg_network} \
            -m {input.mutation_file} \
            -r {input.expr_pcg_tab_file} \
	    -o {params.results_dir} \
            -a {input.cna_pcg_tab} \
            -l {input.cna_pcg_log_tab} \
            -c {params.cohort_name} \
            -x {params.max_conn} \
            -i {params.min_conn} \
            -t {params.conn_weight} \
            -w {params.exp_weight} \
            -d {params.DEG} \
            -z {params.R_scripts}
            """


#######################################
## Generate plots for miRNA analysis ##
#######################################
rule select_miRNA_and_draw_plots:
    """ 
    Select the relevant miRNA genes and draw the plots
    """
    input:
        xseq_trans_rdata = os.path.join(RESULTS_DIR, "xseq", "miRNA", "trans", "RData", "xseq_trans_miRNA_TFBS_{cohort}.Rdata"), \
        xseq_post_rdata = os.path.join(RESULTS_DIR, "xseq", "miRNA", "trans", "RData", "xseq_trans_miRNA_TFBS_post_prob_{cohort}.Rdata"), \      
        net_rdata = os.path.join(RESULTS_DIR, "xseq", "miRNA", "trans", "RData", "{cohort}_miRNA_network_filtered.Rdata"), \
        expr_tab = config["miRNAseq_tab"], \
        gene_lists = config["gene_lists_file"]
    output:
        drivers_miRNA = os.path.join(RESULTS_DIR, "selected_genes_and_plots", "miRNA", "selected_samples", "Filtered_xseq_post_prob_{cohort}_miRNAs.tab"), \
        dysreg_genes_miRNA = os.path.join(RESULTS_DIR, "selected_genes_and_plots", "miRNA", "filtered_networks", "Dysregulated_genes_{cohort}_miRNAs.tab")
    message:
        "; Visualize results xseq trans for miRNA genes"
    params:
        results_dir = os.path.join(RESULTS_DIR, "selected_genes_and_plots", "miRNA"), \
        R_scripts = config["R_scripts"], \
        cohort_name = config["cohort_name"], \
        gene_type = "miRNA", \
        driver_prob = config["driver_gene_prob"], \
        driver_mut = config["driver_mut_prob"], \
        dysreg_gene = config["dysreg_gene_prob"]
    shell:
        """
        Rscript {params.R_scripts}/dysmiR_plots.R  \
        -o {params.results_dir} \
        -c {params.cohort_name} \
        -x {input.xseq_trans_rdata} \
        -p {input.xseq_post_rdata} \
        -n {input.net_rdata} \
        -e {input.expr_tab} \
        -d {params.driver_prob} \
        -m {params.driver_mut} \
        -t {params.gene_type} \
        -l {input.gene_lists} \
        -g {params.dysreg_gene} \
        -z {params.R_scripts}
         """


#######################################
## Generate plots for miRNA analysis ##
#######################################
rule select_PCG_and_draw_plots:
    """ 
    Select the relevant protein coding genes and draw the plots
    """
    input:
        xseq_trans_rdata = os.path.join(RESULTS_DIR, "xseq", "PCG", "trans", "RData", "xseq_trans_PCG_TFBS_{cohort}.Rdata"), \
        xseq_post_rdata = os.path.join(RESULTS_DIR, "xseq", "PCG", "trans", "RData", "xseq_trans_PCG_TFBS_post_prob_{cohort}.Rdata"), \      
        net_rdata = os.path.join(RESULTS_DIR, "xseq", "PCG", "trans", "RData", "{cohort}_PCG_network_filtered.Rdata"), \
        expr_tab = config["RNAseq_tab"], \
        gene_lists = config["gene_lists_file"]
    output:
        drivers_miRNA = os.path.join(RESULTS_DIR, "selected_genes_and_plots", "PCG", "selected_samples", "Filtered_xseq_post_prob_{cohort}_PCGs.tab"), \
        dysreg_genes_miRNA = os.path.join(RESULTS_DIR, "selected_genes_and_plots", "PCG", "filtered_networks", "Dysregulated_genes_{cohort}_PCGs.tab")
    message:
        "; Visualize results xseq trans for PCG genes"
    params:
        results_dir = os.path.join(RESULTS_DIR, "selected_genes_and_plots", "PCG"), \
        R_scripts = config["R_scripts"], \
        cohort_name = config["cohort_name"], \
        gene_type = "PCG", \
        driver_prob = config["driver_gene_prob"], \
        driver_mut = config["driver_mut_prob"], \
        dysreg_gene = config["dysreg_gene_prob"]
    shell:
        """
        Rscript {params.R_scripts}/dysmiR_plots.R  \
        -o {params.results_dir} \
        -c {params.cohort_name} \
        -x {input.xseq_trans_rdata} \
        -p {input.xseq_post_rdata} \
        -n {input.net_rdata} \
        -e {input.expr_tab} \
        -d {params.driver_prob} \
        -m {params.driver_mut} \
        -t {params.gene_type} \
        -l {input.gene_lists} \
        -g {params.dysreg_gene} \
        -z {params.R_scripts}
         """
        

###########################################
## GO and GSEA of the dysregulated genes ##
###########################################

rule GO_GSEA_enrichment_per_cohort:
    """ 
    Create a table with the N most enriched terms + barplots
    """
    input:
       os.path.join(RESULTS_DIR, "selected_genes_and_plots", "{genes}", "filtered_networks", "Dysregulated_genes_{cohort}_{genes}s.tab")
    output:
        os.path.join(RESULTS_DIR, "functional_enrichment_analysis", "{genes}", "tables", "Enriched_terms_{genes}_{cohort}.tab")
    message:
        "; Running enrichR on the set of dysregulated genes: {wildcards.genes}"
    params:
        results_dir = os.path.join(RESULTS_DIR, "functional_enrichment_analysis", "{genes}"), \
        R_scripts = config["R_scripts"], \
        cohort_name = config["cohort_name"], \
        topn = config["top_n_terms"], \
        pval = config["pvalue_enrich"]       
    shell:
        """
        Rscript {params.R_scripts}/GO_GSEA_analysis_enrichR.R \
        -o {params.results_dir} \
        -l {input} \
        -n {params.topn} \
        -p {params.pval} \
        -c {params.cohort_name} \
        -t {wildcards.genes} 
        """


########################
## Interactive report ##
########################
rule dysmiR_interactive_report:
    """ 
    Create an interactive report in html with plots and tables
    """
    input:
       enrichr = os.path.join(RESULTS_DIR, "functional_enrichment_analysis", "{genes}", "tables", "Enriched_terms_{genes}_{cohort}.tab"), \
       template = os.path.join("R-scripts", "Report_dysmiR.Rmd")
    output:
        os.path.join(RESULTS_DIR, "Report", "{genes}", "dysmiR_analysis_report_{cohort}_{genes}.html")
    message:
        "; Generating interactive report for: {wildcards.genes}"
    params:
        results_dir = os.path.join(RESULTS_DIR, "functional_enrichment_analysis", "{genes}"), \
        R_scripts = config["R_scripts"], \
        cohort_name = config["cohort_name"], \
        analysis_name = config["title"], \
        input_dir_abs = MAIN_DIR_ABS, \
     
    shell:
        """
        Rscript {params.R_scripts}/Generate_report.R \
        -c {params.cohort_name} \
        -a {params.analysis_name} \
        -i {params.input_dir_abs} \
        -r {input.template} \
        -g {wildcards.genes}
        """

#####################
## Load R packages ##
#####################
required.libraries <- c("optparse")

for (lib in required.libraries) {
  if (!require(lib, character.only=TRUE)) {
    install.packages(lib)
    suppressPackageStartupMessages(library(lib, character.only=TRUE))
  }
}



####################
## Read arguments ##
####################
option_list = list(

  make_option(c("-c", "--cohort_name"), type="character", default = NULL, 
              help = "Cohort name. (Mandatory) ", metavar="character"),  
  
  make_option(c("-a", "--analysis_name"), type="character", default = NULL, 
              help = "Analysis name. (Mandatory) ", metavar="character"),  
  
  make_option(c("-i", "--input_directory_abs"), type="character", default = NULL, 
              help = "Absolute path to the main directory (where the Snakefile is; Mandatory)", metavar="character"), 
  
  make_option(c("-r", "--report_template"), type="character", default = NULL, 
              help = "Absolute path to thereport tamplate (R-scripts/Report_dysmiR.Rmd; Mandatory)", metavar="character"), 
  
  make_option(c("-g", "--gene_type"), type="character", default = NULL, 
              help = "Gene type [ PCG | miRNA ]", metavar="character")

  );
message("; Reading arguments from command line")
opt_parser = OptionParser(option_list = option_list);
opt = parse_args(opt_parser);



########################
## Set variable names ##
########################
cohort.name            <- opt$cohort_name
analysis.name          <- opt$analysis_name
main.dir               <- opt$input_directory_abs
report.template.file   <- opt$report_template
gene.type              <- opt$gene_type

report.dir <- file.path(main.dir, analysis.name, "results", "Report", gene.type)
dir.create(report.dir, showWarnings = F)


## Example
# cohort.name            <- "LIHC-US"
# main.dir               <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/dysmir/dysmir_pipeline"
# report.template.file   <- "/home/jamondra/Documents/PostDoc/Mathelier_lab/Projects/dysmir/dysmir_pipeline/Test_report/Report_dysmiR.Rmd"
# analysis.name          <- "dysmiR_example"
# gene.type              <- "PCG"
# gene.type              <- "miRNA"


########################################
## Generate list with files and alias ##
########################################
message("; Generating list of input files to complete the report")
alias.to.files <- list()
alias.to.files[["--two_comp--"]]                   <- file.path(main.dir, analysis.name, "results/xseq", gene.type, "trans/plots", paste0(cohort.name, "_weights_distrib_xseq_trans_", gene.type, ".png"))
alias.to.files[["--post_distrib_plot--"]]          <- file.path(main.dir, analysis.name, "results/selected_genes_and_plots", gene.type, "Rdata", paste0("Gene_post_mut_prob_distribution_", cohort.name, ".Rdata"))
alias.to.files[["--ranked_genes--"]]               <- file.path(main.dir, analysis.name, "results/selected_genes_and_plots", gene.type, "Rdata", paste0("Ranked_", gene.type, "s_", cohort.name, "_mutation_type_TFBS.RData"))
alias.to.files[[ "--fraction_dyreg_net_plot--"]]   <- file.path(main.dir, analysis.name, "results/selected_genes_and_plots", gene.type, "Rdata", paste0("Dysregulated_network_fraction_", cohort.name, ".Rdata"))
alias.to.files[[ "--shared_dysreg_plot--"]]        <- file.path(main.dir, analysis.name, "results/selected_genes_and_plots", gene.type, "Rdata", paste0("Pairwise_shared_dysregulated_genes_", cohort.name, ".Rdata"))
alias.to.files[["--dysreg_net_frac_tab--"]]        <- file.path(main.dir, analysis.name, "results/selected_genes_and_plots", gene.type, "filtered_networks", paste0("Dysregulated_network_fraction_", cohort.name, ".Rdata"))
alias.to.files[["--ranked_terms--"]]               <- file.path(main.dir, analysis.name, "results/functional_enrichment_analysis", gene.type, "RData", paste0("Ranking_enrichment_terms_plots_", cohort.name, ".RData"))
alias.to.files[["--enrichr_tab--"]]                <- file.path(main.dir, analysis.name, "results/functional_enrichment_analysis", gene.type, "RData", paste0("All_terms_", gene.type, "_", cohort.name, ".Rdata"))
alias.to.files[["--xseq_tab--"]]                   <- file.path(main.dir, analysis.name, "results/xseq", gene.type, "trans/RData", paste0("xseq_trans_", gene.type, "_TFBS_", cohort.name, ".Rdata"))
alias.to.files[["--dysreg_heatmaps_folder--"]]     <- file.path(main.dir, analysis.name, "results/selected_genes_and_plots", gene.type, "dysregulation_heatmaps")

## For the moment aggregate networks only works in PCG analysis
agg.net.flag <- NULL
if (gene.type == "PCG") {
  alias.to.files[["--aggregate_network_plot--"]]   <- file.path(main.dir, analysis.name, "results/selected_genes_and_plots", gene.type, "Rdata", paste0("Aggregated_network_", cohort.name, ".Rdata"))
  agg.net.flag <- file.exists(alias.to.files[["--aggregate_network_plot--"]])
  
  if (!agg.net.flag) {
     alias.to.files <- within(alias.to.files, rm("--aggregate_network_plot--"))
  }
}



message("; Checking all the input files exist")
input.files.exist <- sapply(alias.to.files, file.exists)
if (!all(input.files.exist)) {
  
  message("; The following files were not found: ")
  print(as.vector( unlist( alias.to.files[which(input.files.exist == F)] ) ))
  stop("; Files not found")
  
} else {
  message("; All input files exist")
}


##############################
## Fill the report template ##
##############################
message("; Creating a copy of the template")
report.to.fill.file <- file.path(report.dir, paste0("dysmiR_analysis_report_", cohort.name, "_", gene.type, "tmp.Rmd"))
file.remove(report.to.fill.file)

## Copy template and read copy
file.copy(from = report.template.file,
          to = report.to.fill.file)

report.to.fill <- readLines(report.to.fill.file)

for (alias.n in names(alias.to.files)) {

  message("; Adding content to the HTML report: ", alias.to.files[[alias.n]])
  
  report.to.fill <- gsub(report.to.fill,
                     pattern = alias.n,
                     replacement = alias.to.files[[alias.n]], ignore.case = T, perl = T)
  
  ## Uncomment section
  if (gene.type == "PCG" & agg.net.flag == TRUE) {

    report.to.fill <- gsub(report.to.fill,
                           pattern = "^<!--\\s+#\\s+",
                           replacement = "",
                           perl = T)
    
    report.to.fill <- gsub(report.to.fill,
                           pattern = "\\s*-->$",
                           replacement = "",
                           perl = T)
  }
    
}

report.ready.file <- file.path(report.dir, paste0("dysmiR_analysis_report_", cohort.name, "_", gene.type, ".Rmd"))
file.remove(report.ready.file)
writeLines(report.to.fill, report.ready.file)
file.remove(report.to.fill.file)


##########################
## Generate html report ##
##########################
rmarkdown::render(report.ready.file)

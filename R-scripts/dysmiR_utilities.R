#############################
## Load required libraries ##
#############################
required.libraries <- c("circlize",
                        "ComplexHeatmap",
                        "dplyr",
                        "ggplot2",
                        "xseq")
for (lib in required.libraries) {
  if (!require(lib, character.only = TRUE)) {
    install.packages(lib)
    suppressPackageStartupMessages(library(lib, character.only = TRUE))
  }
}


#################################
## Generate randomized network ##
#################################
# 
# network <- subset(miR.mRNA.network, Weight_context_score_perc >= p & miR == mir)

## Given a dataframe representing a network with two columns: (1) Feature and (2) Target
## Return n networks with the same size and number of interaction for each feature.
## The interactions are randomly selected.
## User can control:
## 1) Target replacement (applied independently on any of the n networks)
## 2) No overlap between the input targets with the randomized targets
randomized.network <- function(network, n = 1, replace.element = FALSE, noovlp = TRUE){
  
  ## In case the network has more than two columns, keep only columns 1 and 2
  network <- network[,c(1:2)]
  
  ## Remove duplicated lines
  network <- unique.data.frame(network)
  
  ## Rename network columns
  colnames(network) <- c("Feature", "Target")
  
  ## Get the feature names
  features <- unique(unlist(network[,"Feature"]))
  nb.features <- length(features)
  
  ## Get the target names
  targets <<- unique(unlist(network[,"Target"]))
  nb.targets <- length(targets)
  
  ## For each feature, count the number of targets
  nb.targets.per.feature <<- table(network[,"Feature"])
  
  ## Split the network (data.frame) into a list
  ## Each entry is the feature name
  targets.per.feature <<- split(network, f = network$Feature)
  
  target.names.per.feature <<- sapply(targets.per.feature, function(df){
    unique(as.vector(unlist(df[,"Target"])))
  })
  
  
  ## The randomized networks are generated n times
  ## Default: 1
  randomized.network.df <- data.frame()
  for(i in 1:n){
    
    ## Iterate on each feature
    
    for(f in features){
      
      ## Generate one random network for a given feature
      random.network.f <- NULL
      random.network.f <- randomized.network.one.feature(f, noovlp = noovlp, replace.element = replace.element)
      
      ## Add the column with the network number
      random.network.f$Network_nb <- i
      
      ## Concat the network
      randomized.network.df <- rbind(randomized.network.df, random.network.f)
    }
  }
  
  return(randomized.network.df)
}

## This is the core function
## Generate one random network for only one feature (iterate to generate more networks)
## Number of targets is taken by previously defined objects
randomized.network.one.feature <- function(f, noovlp = TRUE, replace.element = FALSE){
  
  ## Get the number of targets for the query feature
  nb.targets.f <- nb.targets.per.feature[f]
  
  ## Get the target's names of the query feature
  targets.f <- as.vector(unlist(target.names.per.feature[f]))
  
  ## By default the input targets are not chosen in the randomized networks
  ## This paraneter can be set by the user
  if(noovlp == TRUE){
    
    targets.filtered <- targets[!targets %in% targets.f]
    
  } else {
    targets.filtered <- targets
  }
  
  ## Generate the random targets
  ## Note: replace parameter can be set by the user
  random.targets.f <- sample(targets.filtered, size = nb.targets.f, replace = replace.element)
  
  ## Create a data.frame with the results
  ## Add the feature + nb network colummns
  randomized <- data.frame(Feature = f, Target = random.targets.f)
  randomized$Feature <- as.vector(randomized$Feature)
  randomized$Target <- as.vector(randomized$Target)
  
  as.data.frame(randomized)
}


####################################
## Generate network (xseq format) ##
####################################
## Convert a data.frame network (mir, target, score) in a list of lists.
## Each sub-list is names as the miR and their content correspond to the association values of the target genes.
df.net.to.xseq <- function(network){

  ## Rename columns
  colnames(network) <- c("Gene", "Target_gene", "Assoc_score")

  ##
  net.list <- split(network, f = network$Gene)

  xseq.net <- lapply(net.list, function(l){

    scores <- l$Assoc_score
    names(scores) <- l$Target_gene

    l <- scores

  })

  return(xseq.net)

}


xseq.net.to.df <- function(net) {
  
  gene.names <- names(net)
  
  net.df <- lapply(seq_along(gene.names), function(i){
    
    df <- data.frame(net[[i]])
    
    df <- df %>% 
      mutate(Gene = gene.names[i], Target = rownames(df))
    df <- df[,c(2,3,1)]
    colnames(df) <- c("Gene", "Target", "Weight")
    
    df
  })
  
  net.df <- do.call(rbind, net.df)
  
  return(net.df)
}


########################################
## Add columns to the premiRNAs names ##
########################################
## NOTE: the colnames in the miRNa expression table correspond to the premiRNAs, not to the
## mature miRNAS. This must be adapted because the networks are based on the mature miRNAs.
rename.add.columns.mature.mir <- function(tab, mir.dic){
  
  ## Transpose the matrix to apply the merge function using a column with the premiR names
  tab <- t(tab)
  tab <- as.data.frame(tab)
  
  ## Add this column to further use the merge function
  tab$premiR <- rownames(tab)
  
  ## Merge the expression table with the miR <-> premiR dictionary
  tab <- merge(tab, mir.dic, by = "premiR")
  
  mir.ids <- tab$mature_ID
  ## Remove the miR and premiR columns
  tab <- within.data.frame(tab, rm(premiR, miR, mature_ID))
  
  ## Rename the columns with the mature miRNA names.
  ## Trick: this is done after transposing, because colnames accepts repeated names, whilst rownames does not.
  tab <- data.frame(t(tab))
  colnames(tab) <- mir.ids
  
  return(tab)
}


###########################################################
## Convert the gene posterior probability in a dataframe 
pg.to.df <- function(post.prob.tab, gene.name){
  
    # print(names(post.prob.tab$posterior.g))
    # print(gene.name %in% names(post.prob.tab$posterior.g))
    
    ## Clean names, mature::precursor (in case of required)
    gene.name <- gsub(gene.name, pattern = "^.+::", replacement = "")
    
    if(gene.name %in% names(post.prob.tab$posterior.g)){

      ## Get the specimen IDs of the mutated samples
      sample.names <- names(post.prob.tab$posterior.g[[gene.name]])
      dysreg.prob.names <- c("Down", "Neutral", "Up")
      
      ## Convert the list in a table
      dysreg.prob.tab <- data.frame(do.call(rbind, post.prob.tab$posterior.g[[gene.name]]))
      colnames(dysreg.prob.tab) <- dysreg.prob.names
      
      ## Add the miRNA + sample IDs
      dysreg.prob.tab$gene <- gene.name
      dysreg.prob.tab$specimen <- sample.names
      
      ## Calculate the max value and sign between the down (-) and Up (+) regulation column
      ## Column 1: Down
      ## Column 2: Neutral
      ## Column 3: Up
      dysreg.prob.parsed <- dysreg.prob.tab[,c(1,3,4,5)] %>% 
        group_by(gene, specimen) %>% 
        summarise(dysreg_prob = max(c(Down, Up)),
                  dysreg_sign = which.max(c(Down, Up)))
      dysreg.prob.parsed$dysreg_sign <- ifelse(dysreg.prob.parsed$dysreg_sign == 1, yes = -1, no = +1)
      
      dysreg.prob.parsed$dysreg_prob <- dysreg.prob.parsed$dysreg_prob * dysreg.prob.parsed$dysreg_sign
      dysreg.prob.parsed$dysreg_prob <- round(dysreg.prob.parsed$dysreg_prob, digits = 2)
      
      return(dysreg.prob.parsed[,c(1:3)])
      
    } else {
      return(NULL)
    }
}


###########################
## Dysregulation heatmap ##
###########################
## Given a gene name, this heatmap shows:
## 1) Heatmap cells: the dysregulation probabilities P(G) of all the connected genes
## 2) Column bar: sample posterior probability of the given gene
## 3) Row bar (Net weight): the connection strengt between the given gene and each target
## 4) Row bar (Expr weight): the relative expression value of the connected gene
dysreg.heatmap <- function(gene,
                          posterior,
                          net,
                          cor.tab = NULL,
                          cis.prob = NULL,
                          target.expr,
                          dysreg.g.th = 0.5,
                          prob.driver = 0.8,
                          main="in_Cancer") {

  dysreg.g.th <- as.numeric(dysreg.g.th)
  prob.driver <- as.numeric(prob.driver)
  
  ## Extract the gene name
  gene = intersect(gene, names(posterior$posterior.g))
  
  if ( (is.null(unlist(posterior$posterior.g[gene])) )  |  (length(gene) == 0) ) {
    
    message("; ", gene, " - No significant dysregulated genes")
    connected.genes <- NULL
    nb.connected.genes <- NULL
    ht <- NULL
    dysreg.mat.filt.melted <- NULL
    
  } else {
    
    ## Get the max dysreg probability for each patient between down(-)/up(+) regulation.
    prob.g = lapply(posterior$posterior.g[gene],
                    function(z) sapply(z, function(z1)
                      sign(z1[,3] - z1[,1]) * pmax(z1[,3],  z1[,1])))
    
    
    if ( nrow(data.frame(prob.g)) < 3 | ncol(data.frame(prob.g)) == 1 ) {
      
      message("; ", gene, " - No significant dysregulated genes")
      connected.genes <- NULL
      nb.connected.genes <- NULL
      ht <- NULL
      dysreg.mat.filt.melted <- NULL
      
    } else {
    
    ## Get F(D)
    post.d <- ConvertXseqOutput(posterior)
    colnames(post.d) <- c("sample", "hgnc_symbol", "prob_sample", "prob_gene")
    
    ## Get the significant samples for each significant gene
    post.d <- post.d %>% 
                dplyr::filter(prob_gene >= prob.driver & prob_sample >= dysreg.g.th & hgnc_symbol == gene) %>% 
                dplyr::select(sample, hgnc_symbol)
    significant.samples <- unique(post.d$sample)
    
    id = sapply(prob.g, function(z) is.matrix(z))
      
    gene = gene[id]
    prob.g = prob.g[id]
    legend.lab <- 20
      
    ## Function to calculate max aboslute
    absmax <- function(x) { x[which.max( abs(x) )]}
      
    ## Show the genes with an absolute max dyreg prob >= 0.5 in the significant samples
    high.dysreg.genes <- abs(apply(prob.g[[gene]][,significant.samples], 1, absmax)) >= dysreg.g.th
      
    if (sum(high.dysreg.genes) >= 5) {  
      
      ## Select the genes significantly dysregulated in two or more samples
      dysreg.mat.filt <- as.data.frame(prob.g[[gene]][high.dysreg.genes,])
      
      ## Convert the actual expression value of the target genes in a Z-score
      ## Use function 'scale'
      mut.samples <- colnames(prob.g[[gene]])
      
      # if( !is.null(target.expr) ) {
      #   target.expr.z <- apply(target.expr, 2, scale, center = T, scale = T)
      #   rownames(target.expr.z) <- rownames(target.expr)
      #   
      #   #   g.parsed <- gsub(gene, pattern = "::.+$",replacement = "")
      #   # g.parsed <- gsub(gene, pattern = "^.+::",replacement = "")
      #   # target.expr.z <- target.expr.z[mut.samples,g.parsed]
      #   
      #   target.expr.z.parsed <- ifelse(target.expr.z > 3, yes = 3, no = target.expr.z)
      #   target.expr.z.parsed <- ifelse(target.expr.z.parsed < -3, yes = -3, no = target.expr.z.parsed)
      # }
      
      #################################
      ## Prepare heatmap annotations ##
      #################################
      cis.post.prob.df <- NULL
      if(!is.null(cis.prob)){
        cis.post.prob.df <- pg.to.df(post.prob.tab = cis.prob,
                                     gene.name = gene)
      }
      cis.post.parsed.tab <- NULL
      
      
      if(!is.null(cis.post.prob.df) & !is.null(target.expr)){
        
        
        cis.post.parsed.tab <- cis.post.prob.df$dysreg_prob
        names(cis.post.parsed.tab) <- cis.post.prob.df$specimen
        
        cis.post.parsed.tab <- cis.post.parsed.tab[names(cis.post.parsed.tab) %in% names(posterior$posterior.f[[gene]][, 2])]
        
        ## Column annotation
        ## Sample posterior probability in the analyzed gene
        sample.post.prob.df <- data.frame(Sample_post_prob = posterior$posterior.f[[gene]][, 2],
                                          Cis_post_prob = cis.post.parsed.tab,
                                          Z_score = as.vector(target.expr.z.parsed))
        
        post_prob_color.palette <- list(Sample_post_prob = colorRamp2(c(0, 0.5, 1), c("#ffffcc", "#fd8d3c", "#b10026")),
                                        Cis_post_prob = colorRamp2(c(-1, -0.75, -0.5, -0.25, 0, 0.25, 0.5, 0.75, 1), rev(c("#d73027", "#f46d43", "#fdae61", "#fee090", "#ffffbf", "#e0f3f8", "#abd9e9", "#74add1", "#4575b4" ))),
                                        Z_score = colorRamp2(c(-3, -2, -1, 0, 1, 2, 3), rev(c("#b2182b", "#d6604d", "#f4a582", "#ffffff", "#bababa", "#878787", "#4d4d4d"))))
        
      } else {
        ## Column annotation
        ## Sample posterior probability in the analyzed gene
        sample.post.prob.df <- data.frame(Sample_post_prob = posterior$posterior.f[[gene]][, 2])
        
        post_prob_color.palette <- list(Sample_post_prob = colorRamp2(c(0, 0.5, 1), c("#edf8e9", "#74c476", "#005a32")))
        
      }
      
      samples.post.prob.bar <- HeatmapAnnotation(df = sample.post.prob.df, col = post_prob_color.palette, border = TRUE, show_annotation_name = F)
      
      
      ## Row annotations
      ## P(H): probability that the mutated samples are down/up regulated
      ph.vector <- apply(posterior$posterior.h[[gene]], 1, function(r){
        
        if (which.max(r) == 1) {
          r[1] <- -r[1]
          
        } else {
          r[2]
        }
        
      })
      
      
      if (!is.null(cor.tab)) {
        
        ## Row annotation: miRNA-target correlation Rho
        mirna.parsed.name <- gsub(gene, pattern = "::.+$", replacement = "")
        
        cor.tab$Spearman <- as.numeric(cor.tab$Spearman)
        cor.tab$Significance <- as.numeric(cor.tab$Significance)
        cor.tab$gene <- as.vector(cor.tab$gene)
        cor.tab$miRNA <- as.vector(cor.tab$miRNA)
        
        cor.tab$Significance <- ifelse(cor.tab$Significance >= 50, yes = 50, no = cor.tab$Significance)
        cor.tab$Significance <- ifelse(cor.tab$Significance < 0, yes = 0, no = cor.tab$Significance)
        cor.tab$Significance <- cor.tab$Significance * sign(cor.tab$Spearman)
        
        cor.tab.filt <- subset(cor.tab[,c("miRNA", "gene", "Spearman", "Significance")], miRNA == mirna.parsed.name & gene %in% rownames(dysreg.mat.filt)) %>%
          group_by(miRNA, gene) %>% 
          filter(Spearman == max(Spearman))
        
        cor.tab.filt <- as.data.frame(cor.tab.filt)
        rownames(cor.tab.filt) <- cor.tab.filt$gene
        
        
        row.annot.df <- data.frame(Prob_dysreg = ph.vector[rownames(dysreg.mat.filt)],
                                   Signif = round(cor.tab.filt[rownames(dysreg.mat.filt), "Significance"], digits = 2))
        row.annot.color.palette <- list(Prob_dysreg = colorRamp2(c(-1, -0.75, -0.5, 0, 0.5, 0.75, 1), c("#8073ac", "#b2abd2", "#f7f7f7", "#f7f7f7", "#f7f7f7", "#fdb863", "#e08214")),  ## Dysregulation color scale
                                        # Signif = colorRamp2(c(-100, -75, -50, -25, 0, 25, 50, 75, 100), rev(c("#d73027", "#f46d43", "#fdae61", "#fee08b", "#ffffbf", "#d9ef8b", "#a6d96a", "#66bd63", "#1a9850"))))
                                        Signif = colorRamp2(c(-50, -25, 0, 25, 50), rev(c("#d73027", "#f46d43", "#ffffbf", "#66bd63", "#1a9850"))))
        
        # Signif = colorRamp2(c(-200, -150, -100, -50, 0, 50, 100, 150, 200), c("#542788", "#8073ac", "#b2abd2", "#d8daeb", "#f7f7f7", "#fee0b6", "#fdb863", "#e08214", "#b35806")))           ## Spearman Rho color scale
        net.expr.row <- HeatmapAnnotation(df = row.annot.df, col = row.annot.color.palette,
                                          which = "row",
                                          width = unit(0.85, "cm"))
      } else {
        row.annot.df <- data.frame(Prob_dysreg = ph.vector)
        row.annot.color.palette <- list(Prob_dysreg = colorRamp2(c(-1, -0.75, -0.5, 0, 0.5, 0.75, 1), c("#8073ac", "#b2abd2", "#f7f7f7", "#f7f7f7", "#f7f7f7", "#fdb863", "#e08214")))    ## Up-regulation color scale
        net.expr.row <- HeatmapAnnotation(df = row.annot.df, col = row.annot.color.palette,
                                          which = "row",
                                          width = unit(0.85, "cm"))
      }
      
      ## Assign colors to the heatmap
      col_fun = colorRamp2(c(-1, 0, 1), c("#2166ac", "white", "#b2182b"))
      
      
      ##########################
      ## Draw complex heatmap ##
      ##########################
      ht = Heatmap(dysreg.mat.filt,
                   name = "Gene\nRegulatory\nStatus",
                   col = col_fun,
                   
                   ## Column annotations
                   top_annotation = samples.post.prob.bar,
                   
                   ## Heatmap title
                   column_title = paste(gene, main, sep = " "),
                   
                   ## Clustering
                   cluster_columns = function(m){ hclust(dist(m), method = "ward.D2")},
                   cluster_rows = function(m){ hclust(dist(m), method = "ward.D2")},
                   
                   # column_names_gp = gpar(cex = 0.5),
                   
                   ## Cell spacing
                   rect_gp = gpar(col = "white", lty = 1, lwd = 2, row = "white")
                   
      )
      ht <- draw(ht, heatmap_legend_side = "right", annotation_legend_side = "right", merge_legend = TRUE)
      
      
      ###### Get the number and names of the dysregulated genes in the network
      connected.genes <- names(which(abs(apply(prob.g[[gene]][,significant.samples], 1, absmax)) >= dysreg.g.th))
      nb.connected.genes <- length(connected.genes)
      
      
      # nb.connected.genes <- nrow(dysreg.mat.filt)
      # connected.genes <- rownames(dysreg.mat.filt)
      
      ## Melt the data frame and add the P(G) information
      dysreg.mat.filt.melted <- melt(as.matrix(dysreg.mat.filt))
      colnames(dysreg.mat.filt.melted) <- c("Target_gene_name", "Specimen", "Dysreg_prob_trans")
      dysreg.mat.filt.melted$Gene <- gene
      
      # if(!is.null(cis.prob)){
      #   ## Add the P(G) cis information
      #   colnames(cis.post.prob.df) <- c("Gene_name", "Specimen", "Dysreg_prob_cis")
      #   dysreg.mat.filt.melted <- merge(dysreg.mat.filt.melted, cis.post.prob.df[,c(2,3)], by = "Specimen")
      # }
      # 
      # 
      # ## Add the correlation test information
      # if(!is.null(cor.tab)){
      #   
      #   dysreg.mat.filt.melted <- merge(dysreg.mat.filt.melted, cor.tab.filt, by.x = "Target_gene_name", by.y = "gene")
      #   # dysreg.mat.filt.melted <- dysreg.mat.filt.melted[,c("Target_gene_name", "Gene", "Specimen", "Dysreg_prob_trans", "Dysreg_prob_cis", "Spearman", "Significance")]
      #   dysreg.mat.filt.melted <- dysreg.mat.filt.melted[,c("Target_gene_name", "Gene", "Specimen", "Dysreg_prob_trans", "Spearman", "Significance")]
      # }
      
      
      # if(!is.null(target.expr)){
      #   ## Add the Z-score information
      #   zscore.df <- data.frame(Z_score_expr = target.expr.z)
      #   zscore.df$Specimen <- rownames(zscore.df)
      #   dysreg.mat.filt.melted <- merge(dysreg.mat.filt.melted, zscore.df, by = "Specimen")
      #   
      #   if(grepl(gene, pattern = "hsa-mir")){
      #     dysreg.mat.filt.melted <- dysreg.mat.filt.melted[,c("Specimen", "Target_gene_name", "Dysreg_prob_trans", "Gene", "Spearman", "Significance", "Z_score_expr")]
      #   }
      # }
    
     
    } else {
      
      message("; ", gene, " - Less than 5 significant dysregulated genes")
      connected.genes <- NULL
      nb.connected.genes <- NULL
      ht <- NULL
      dysreg.mat.filt.melted <- NULL
    }
  
    }
  }
  return(list(prob.g = prob.g,
              legend = legend,
              heat = ht,
              nb_connected_genes = nb.connected.genes,
              connected_genes =  connected.genes,
              summary_tab = dysreg.mat.filt.melted))
}
                

###########################################################
## Return a set of colors for the distinct mutation types
## The idea is to let this color fixed for the plots generated in distinct scripts
assign.colors <- function(data.type){
  
  if(data.type == "premiRNA"){
    
    assigned.colors <- c("#253494", "#7a0177", "#bd0026")
    names(assigned.colors) <- c("TFBS", "premiRNA_+_TFBS", "premiRNA")
    
  }  else if(data.type == "miRNA"){
    
    assigned.colors <- c("#253494", "#7a0177", "#bd0026")
    names(assigned.colors) <- c("TFBS", "miRNA_+_TFBS", "miRNA")
    
  } else if(data.type == "PCG"){
    
    assigned.colors <- c("#253494", "#f16913", "#006837", "#a6761d", "#e6ab02")
    names(assigned.colors) <- c("TFBS", "LoF_+_TFBS", "LoF", "WXS", "WXS_+_TFBS")
    
  } else if(data.type == "PCG_miRNA"){
    
    assigned.colors <- c("#253494", "#7a0177", "#f16913", "#006837", "#a6761d", "#e6ab02")
    names(assigned.colors) <- c("TFBS", "premiRNA_+_TFBS", "LoF_+_TFBS", "LoF", "WXS", "WXS_+_TFBS")
    
  } else if(data.type == "Mutations"){
    
    assigned.colors <- c("#e41a1c", "#377eb8")
    names(assigned.colors) <- c("Mutated", "Non-mutated")

  } else if(data.type == "Cohorts"){
    
    assigned.colors <- c("#1b9e77", "#d95f02", "#7570b3", "#e7298a", "#66a61e", "#e6ab02", "#a6761d")
    names(assigned.colors) <- c("BRCA-US", "LIHC-US", "UCEC-US", "HNSC-US", "STAD-US", "LUAD-US", "LUSC-US")
    
  } else if(data.type == "Gene_types"){
    
    assigned.colors <- c("#a6761d", "#bd0026")
    names(assigned.colors) <- c("PCG", "miRNA")
    
  } else {
    assigned.colors <- NULL
  }
  
  return(assigned.colors)
  
}


## custom blank theme ggplot
blank_theme <- theme_minimal() +
  theme(
    axis.title.x = element_blank(),
    axis.title.y = element_blank(),
    panel.border = element_blank(),
    panel.grid=element_blank(),
    axis.ticks = element_blank(),
    plot.title=element_text(size=14, face="bold")
  )

## Capitalize first letter
firstup <- function(x) {
  substr(x, 1, 1) <- toupper(substr(x, 1, 1))
  x
}



################################################################################
################################################################################
## Customized xseq functions
SpecifyPriorModel = function(p.d, p.df, p.fg) {
  if (missing(p.d)) {
    p.d = structure(c(0.95, 0.05), names=c(0, 1))
  }
  
  if (missing(p.df)) {
    p.df = matrix(c(0.90, 0.10, 0.10, 0.90), 2, 2, 
                  byrow=TRUE, dimnames=list(seq(2)-1, seq(2)-1))
  }
  
  if (missing(p.fg)) {
    p.fg = matrix(c(0.025, 0.95, 0.025, 0.20, 0.60, 0.20), 
                  2, 3, byrow=TRUE, dimnames=list(seq(2)-1, seq(3)-1))
  }
  
  cpd = list(p.d.share=p.d, p.df.share=p.df, p.fg.share=p.fg)
  return (cpd)
}


SetXseqPrior2 = function(mut, expr.dis, net, regulation.direction=TRUE, 
                         cis=TRUE, mut.type="loss", ...) {
  
  cpd = SpecifyPriorModel(...)
  virtual.data.fraction = 0.1  # effective virtual data size is 10%
  
  ## genes with both mutation, expression data and network connections
  gene  = unique(mut[, "hgnc_symbol"])
  
  if (cis == FALSE) {
    count.conn = sapply(net, length)
    gene = intersect(names(count.conn), gene)
    id  = mut[, "hgnc_symbol"] %in% gene
    mut = mut[id, ]
  } else {
    count.conn = setNames(seq(length(gene)), gene)
  }
  
  id  = paste(mut[, "sample"], mut[, "hgnc_symbol"], sep="_")
  id  = !duplicated(id)
  mut = mut[id, ]
  
  count.mut  = table(mut[, "hgnc_symbol"])
  
  print(count.mut)
  
  ## Typically we expect less than 200 high probability genes, 
  beta.pd.share = ceiling(cpd$p.d.share * length(gene) * 
                            virtual.data.fraction)
  if (beta.pd.share[2] == 1) {
    beta.pd.share = beta.pd.share + c(1, 1)
  } else if (beta.pd.share[2] > 200) {
    tmp = beta.pd.share
    tmp[1] = sum(beta.pd.share) - 200
    tmp[2] = 200
    
    cpd$p.d.share = tmp / sum(tmp)
    beta.pd.share = tmp
  }
  
  median.mut = median(count.mut)
  beta.df.share = cpd$p.df.share * cpd$p.d.share * 
    length(gene) * median.mut * virtual.data.fraction
  beta.df.share = ceiling(beta.df.share)
  
  print(beta.df.share)
  if(sum(beta.df.share==1) > 0) {
    beta.df.share = beta.df.share + 1
  }
  
  expr.dis.lambda = expr.dis[[4]]
  lambda = colMeans(expr.dis.lambda)
  lambda.sd = apply(expr.dis.lambda, 2, sd)
  
  p.fg.plus  = lambda + lambda.sd * c(0, 1, 0)
  p.fg.plus  = p.fg.plus / sum(p.fg.plus)
  
  if (regulation.direction == TRUE | 
      (mut.type %in% c("gain", "loss") & cis == TRUE)) {
    p.fg.h.plus  = p.fg.plus
    p.fg.h.plus[3] = p.fg.h.plus[2] = (1 - p.fg.h.plus[1]) / 2
    
    p.fg.h.minus = p.fg.plus
    p.fg.h.minus[1] = p.fg.h.minus[2] = (1 - p.fg.h.minus[3]) / 2
  } else {
    p.fg.minus = lambda + 5 * lambda.sd * c(1, 0, 1)
    p.fg.minus = p.fg.minus / sum(p.fg.minus)
    p.fg.share = rbind(p.fg.plus, p.fg.minus)
  }
  
  if (cis == TRUE) {
    if (mut.type == "loss") {
      p.fg.share = rbind(p.fg.plus, p.fg.h.minus)
    } else if (mut.type == "gain") {
      p.fg.share = rbind(p.fg.plus, p.fg.h.plus)
    }
  }
  
  if (regulation.direction == FALSE | cis == TRUE) {
    dir.fg.share = ceiling(cpd$p.df.share %*% 
                             p.fg.share * cpd$p.d.share * 
                             length(gene) * median.mut * median(count.conn) * 
                             virtual.data.fraction)
  } else {
    p.fg.share = rbind(p.fg.plus, p.fg.h.minus, p.fg.h.plus)
    dir.fg.minus = cpd$p.df.share %*% 
      p.fg.share[1:2, ] * cpd$p.d.share * 
      length(gene) * median.mut * median(count.conn) * 
      virtual.data.fraction
    dir.fg.plus  = cpd$p.df.share %*% 
      p.fg.share[c(1,3), ] * cpd$p.d.share * 
      length(gene) * median.mut * median(count.conn) * 
      virtual.data.fraction
    
    dir.fg.share = rbind((dir.fg.minus[1,] + dir.fg.plus[1,])/2, 
                         dir.fg.minus[2,], 
                         dir.fg.plus[2,])
    dir.fg.share = ceiling(dir.fg.share)
  }
  cpd$p.fg.share = p.fg.share
  
  if(sum(dir.fg.share==1) > 0) {
    dir.fg.share = dir.fg.share + 1
  }
  
  prior = setNames(vector("list", 3), 
                   c("beta.pd.share", "beta.df.share", "dir.fg.share"))
  prior$beta.pd.share = beta.pd.share
  prior$beta.df.share = beta.df.share
  prior$dir.fg.share  = dir.fg.share
  
  return(list(prior=prior, cpd=cpd, baseline=lambda[2]))
}


###########################################
## Posterior probabilities distributions ##
###########################################
post.probs.distrib.plot <- function(xseq.tab = xseq.tab,
                                    data.type = "gene"){
  
  post.probs.distrib.ggplot <- NULL

  if( data.type == "gene" ){
    
    post.probs.distrib.ggplot <- 
      xseq.tab %>% 
        select(gene, prob_gene, mut_effect) %>% 
        unique() %>% 
      ggplot(aes(x = prob_gene, fill = mut_effect)) +
      # geom_density(adjust = 0.25, alpha = 0.35) +
      geom_histogram(binwidth = 0.05, alpha = 0.75) +
      scale_fill_manual(breaks = rev(names(assign.colors("PCG_miRNA"))),
                        values = assign.colors("PCG_miRNA")) +
      theme_bw() +
      geom_vline(aes(xintercept = driver.gene.thr), linetype = "longdash") +
      labs(title = "", x = "Posterior probability - Driver gene", y = "Frequency") +
      theme(legend.title = element_blank()) +
      # theme(legend.position = "left") +
      theme(legend.position = "none")
    
  } else if( data.type == "sample" ){
    
    post.probs.distrib.ggplot <- 
      xseq.tab %>% 
      ggplot(aes(x = prob_mut, fill = mut_effect)) +
        # geom_density(adjust = 0.25, alpha = 0.35) +
        geom_histogram(binwidth = 0.05, alpha = 0.75) +
        scale_fill_manual(breaks = rev(names(assign.colors("PCG_miRNA"))),
                          values = assign.colors("PCG_miRNA")) +
        theme_bw() +
        geom_vline(aes(xintercept = 0.5), linetype = "longdash") +
        labs(title = "", x = "Posterior probability - Driver mutation", y = "") +
        theme(legend.title = element_blank()) +
        theme(legend.position = "none")
    
  } else {
    
    stop("; Incorrect data.type argument. Options: gene | sample")
  }
  
  return(post.probs.distrib.ggplot)

}


#######################
## Ranked genes plot ##
#######################
ranked.genes.plot <- function(xseq.tab = xseq.tab,
                              data.type = "PCG",
                              mut.type = "TFBS",
                              cohort.name = cohort.name,
                              rdata = F){
  
  mirna.ranks.plot <- 
    ggplot(effect.selected.genes.rank.tab, aes(x = Rank, y = prob_gene, colour = Mutated_samples, label = gene)) +
      geom_line(data = effect.selected.genes.rank.tab, aes(x = Rank, y = prob_gene), inherit.aes = F, color = '#9ecae1', alpha = 0.35) + 
      geom_point() +
      theme_bw() +
      theme(text = element_text(size=12),
            axis.text.x = element_text(angle=45, hjust=1, size = 12),
            axis.text.y = element_text(hjust=1, size = 12)) +
      xlab("Ranks") +
      ylab("Posterior probability - Driver gene") +
      geom_hline(aes(yintercept = driver.gene.thr), linetype="dashed", alpha = 0.5) +
      # scale_colour_viridis_c(option = "cividis", direction = -1) +
      scale_colour_gradient(low = "#7fcdbb", high = "#253494") +
      ggtitle(paste0("Highlighted ", data.type,"s in ", cohort.name, " - ", mut.type)) +
      theme(legend.position="bottom") +
      theme(plot.title = element_text(hjust = 0.5)) +
    scale_y_continuous(breaks=c(0, 0.25, 0.5, 0.75, 1)) +
      ylim(c(0,1.1))
  
  if( rdata == F) {
    
    mirna.ranks.plot <- mirna.ranks.plot +
                          # scale_colour_gradient(low = "#7fcdbb", high = "#253494") +
                          geom_label_repel(aes(label = Lab),
                                           box.padding = 0.1,
                                           # point.padding = 0.55,
                                           # direction = "both",
                                           force = 40,
                                           size = 3.5,
                                           color = "black",
                                           max.iter = 20000,
                                           # nudge_x = 50,
                                           # nudge_y = 0.75,
                                           segment.color = '#d9d9d9')
  }
  
  return(mirna.ranks.plot)
}


xseq.to.df.network <- function(network.filt) {
  
  i <- 0
  df.all.nets <- lapply(network.filt, function(l){
    
    i <<- i + 1
    
    gene <- names(network.filt[i])
    
    
    df.net <- data.frame(l)
    df.net$G2 <- rownames(df.net)
    df.net$G1 <- gene
    rownames(df.net) <- NULL
    
    df.net %>% 
      dplyr::rename("Assoc_score" = l) %>% 
      dplyr::select(c("G1", "G2", "Assoc_score"))
    
    
  })
  df.all.nets <- do.call(rbind, df.all.nets)
  rownames(df.all.nets) <- NULL
  
  return(df.all.nets)
}


#######################################################
## Count pair-wise intersection in the list elements ##
#######################################################
##
## Taken from: https://stackoverflow.com/questions/24614391/intersect-all-possible-combinations-of-list-elements
listIntersect <- function(inList) {
  X <- crossprod(table(stack(inList)))
  X[lower.tri(X)] <- NA
  diag(X) <- NA
  out <- na.omit(data.frame(as.table(X)))
  out[order(out$ind), ]
}



###################################
## Simple and list modes
##
## For each name in the Regulator column, it samples X targets from a list of genes
simple.rand.net <- function(net.df, targets) {
  
  message("; Generating a random network using the 'simple' approach")
  
  ## Count the number of targets per regulators
  targets.per.regulator.tab <- net.df %>% 
    group_by(Regulator) %>% 
    tally() %>% 
    arrange(desc(n))
  
  ## Sample the targets from the pool
  new.targets.list <- vector(mode = "list", length = nrow(targets.per.regulator.tab))
  new.targets.list <- sapply(targets.per.regulator.tab$n, function(n){
    sample(target.vec, size = n, replace = T)
  })
  
  ## Convert the list object into a data.frame
  rand.net.df <- do.call(rbind, lapply(new.targets.list, data.frame))
  colnames(rand.net.df) <- "Target"
  
  ## Repeat each regulator name
  regulator.name.vec <- rep(targets.per.regulator.tab$Regulator, times = targets.per.regulator.tab$n)
  
  ## Add the regulator column
  if (nrow(rand.net.df) == length(regulator.name.vec)) {
    rand.net.df$Regulator <- regulator.name.vec
    rand.net.df <- rand.net.df %>% 
      select(Regulator, Target)
    
  } else {
    rand.net.df <- NULL
    stop("; Number of targets and random net table size do not coincide")
  }
  
  return(rand.net.df)
}


###################################
## Shuffle mode
##
## Redistribute the original targets in the network
##
## It may crash when the networks are not ordered by decreasing size
##
## It is not a simple shuffling because a target may be assigned more than once
## to a particular regulator
shuffle.rand.net <- function(net.list = net.list,
                             targets = target.vec) {
  
  message("; Generating random network by shuffling original targets")
  
  all.targets <- target.vec
  new.targets.list <- vector(mode = "list", length = length(net.list))
  new.targets.list <- lapply(net.list, function(l){
    
    message("; Number of targets to reallocate: ", length(all.targets))
    
    ## Network size/Number of targets
    nb.entries <- nrow(l)
    
    regulator.name   <- unique(as.vector(l$Regulator))
    
    ## Get a new set of non-duplicated target genes
    ##
    ## Get a random set of non-duplicated entries
    ## Note that the randomly selected targets may overlap with the original network
    new.targets <- unique(all.targets)[1:nb.entries]
    
    
    ## Update the targets' vector
    new.targets.ind <- match(new.targets, all.targets)
    all.targets  <<- all.targets[-new.targets.ind]
    
    
    data.frame(Regulator = unique(l$Regulator),
               Target    = new.targets)
  })
  
  ## Convert the list object into a data.frame
  rand.net.df <- do.call(rbind, new.targets.list)
  
  return(rand.net.df)
  
}